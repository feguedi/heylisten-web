import React from 'react';
import './App.css';
import Vista from './Components/Vista'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { theme, ThemeProvider } from '@chakra-ui/core'
import { NavBar } from './Components/NavBar'
import Home from './pages/Home'
import SignUp from './pages/SignUp'
import Report from './pages/Report'
import Reports from './pages/Reports'
import Profile from './pages/Profile'
import Login from './pages/Login';

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <ThemeProvider theme={ theme }>
                    <NavBar />
                    <Switch>
                        <Route path="/" component={ Home } exact />
                        <Route path="/login" component={ Login } />
                        <Route path="/signup" component={ SignUp } />
                        <Route path="/reports" component={ Reports } />
                        <Route path="/report/:id" component={ Report } exact />
                        <Route path="/profile" component={ Profile } />
                        <Route path="/vista" component={ Vista } />
                    </Switch>
                </ThemeProvider>
            </div>
        </BrowserRouter>
    );
}

export default App;
