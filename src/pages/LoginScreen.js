import React from 'react'
import { theme, ThemeProvider, Box, ButtonGroup, Button, Flex, Input, FormHelperText, FormControl, FormLabel, FormErrorMessage } from '@chakra-ui/core'
import { useFormik, Formik, Field } from 'formik'
import { REACT_APP_HOST } from '../config'
import axios from 'axios'

const validatePassword = val => {
    let error
    if (!val) {
        error = "La contraseña es requerida"
    }
    return error
}
const validateNickname = val => {
    let error
    if (!val) {
        error = "El nickname es requerido"
    }
    return error
}

const handleLogin = async e => {
    e.preventDefault()
    console.log(`Peticion a ${ REACT_APP_HOST }/login`)
    console.log(`Tipo: ${ typeof(e.target) }`)
    console.log(`Cuerpo: { nickname: ${ e.target.nickname.value }, password: ${ e.target.password.value } }`)
    await axios.post(`${ REACT_APP_HOST }/login`, { nickname: e.target.nickname.value, password: e.target.password.value })
        .then(res => {
            sessionStorage.setItem("token", res.data.token)
            sessionStorage.setItem("usuarioId", res.data.id)
            console.log(`token: ${ sessionStorage.getItem('token') }`)
            setTimeout(() => {
                document.location = "/profile"
            }, 500)
        })
        .catch(err => console.log(`Error: ${ err.message }`))
}

export const LoginScreen = props => {
    console.log(`LoginScreen: props: ${ JSON.stringify(props) }`)
    console.log(`Host: ${ REACT_APP_HOST }`)
    // const formik = useFormik({
    //     onSubmit: props.handleSubmit
    // })

    return (
    <ThemeProvider theme={ theme }>
        <Box borderWidth={ 1 } rounded="lg" overflow="hidden">
            <Formik 
                initialValues={{ nickname: '', password: '', error: false }}
                
                justifyContent="center"
                render={(
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting) => 
                    <form onSubmit={ handleLogin }>
                        <Field name="nickname" validate={ validateNickname }
                            render={({ field, form }) => (
                                <FormControl isInvalid={ form.errors.nickname && form.touched.nickname }>
                                    <FormLabel htmlFor="nickname">Nickname</FormLabel>
                                    <Input {...field} id="nickname" placeholder="Nickname" />
                                    <FormErrorMessage>{ form.errors.nickname }</FormErrorMessage>
                                </FormControl>
                            )}
                        />
                        <Field name="password" validate={ validatePassword }
                            render={({ field, form }) => (
                                <FormControl isInvalid={ form.errors.password && form.touched.password }>
                                    <FormLabel htmlFor="password">Contraseña</FormLabel>
                                    <Input {...field} id="password" placeholder="Contraseña" type="password" />
                                    <FormErrorMessage>{ form.errors.password }</FormErrorMessage>
                                </FormControl>
                            )}
                        />
                            {/* <Input id="nickname" name="nickname" type="text" placeholder="Nickname" size="md" onChange={ props.handleOnChange }></Input> */}
                            {/* <Input id="password" name="password" type="password" placeholder="Contraseña" size="md" onChange={ props.handleOnChange }></Input> */}
                        <Flex align="center" justify="center">
                            <ButtonGroup spacing={ 4 }>
                                <Button mt={ 4 } variantColor="green" type="submit" size="lg" isLoading={ isSubmitting }>Entrar</Button>
                                <Button mt={ 4 } variantColor="gray" size="lg" type="button" onClick={e => document.location="/signup"}>Crear usuario</Button>
                            </ButtonGroup>
                        </Flex>
                        <FormHelperText style={{ color: "tomato" }}>{ props.errMsg }</FormHelperText>
                    </form>
                }
            />
        </Box>
    </ThemeProvider>
)}