import React, { Component } from 'react'
import { Box, Flex, SimpleGrid, Text } from '@chakra-ui/core'
import FontAwesome from 'react-fontawesome'
import axios from 'axios'
import { Mapa } from '../Components/Mapa'
import { REACT_APP_HOST } from '../config'

export default class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            nombre: "", nickname: "", token: "", reportes: [],
            isLoading: true, error: false, errData: ""
        }
    }

    componentDidMount = async () => {
        const token = sessionStorage.getItem("token")
        console.log(`token: ${ token }`)
        console.log(`host: ${ REACT_APP_HOST }`)
        await axios.get(`${ REACT_APP_HOST }/user`, { headers: {
            "Authorization": `Bearer ${ token }`, "Content-Type": "application/x-www-form-urlencoded"
        }})
            .then(res => {
                const { nombre, nickname } = res.data.datos_usuario
                const { reportes } = res.data
                this.setState({ nombre, nickname, reportes, isLoading: false, token })
            })
            .catch(err => {
                console.log(`Datos del error: ${ JSON.stringify(err.response.data) }`)
                console.log(`Estado del error: ${ JSON.stringify(err.response.status) }`)
                console.log(`Respuesta: ${ err.response.data.message }`)
                this.setState({ isLoading: false, error: true, errData: { message: err.response.data.message, status: err.response.status } })
            })
    }

    render() {
        const { nombre, nickname, reportes, token, error, errData, isLoading } = this.state

        if (error) {
            return (<div>
                <h4>Error { errData.status }</h4>
                <p>{ errData.message }</p>
            </div>)
        }

        if (isLoading) {
            return (
                <div>
                    <div style={{ width: "100%", height: "100%", backgroundColor: "#ffffff44", z: 10 }}>
                        <FontAwesome style={{ position: "absolute", right: "48%", top: "48%" }} name="spinner" spin size="5x" />
                    </div>
                </div>
            )
        }

        if (!token) {
            return (<div>
                <h3>No has iniciado sesión</h3>
            </div>)
        }

        return (
        <SimpleGrid columns={ 1 } spacing={ 10 }>
            <Flex align="center" justify="flex-start">
                <Box w="100%">
                    <Text>Nickname: { nickname }</Text>
                    <Text>Nombre: { nombre }</Text>
                </Box>        
            </Flex>
            <Flex>
                <Box align="center" w="100%" rounded="lg" maxH="sm">
                    <Mapa markers={ reportes } height={ window.innerHeight - (window.innerHeight * 0.2) } />
                </Box>
            </Flex>
        </SimpleGrid>
        )
    }
}
