import React, { Component } from 'react'
import { Box, Flex } from '@chakra-ui/core'
import { LoginScreen } from './LoginScreen'
import { APP_HOST } from '../config'
import axios from 'axios'

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = { isLoading: true, error: false, errMsg: "" }
    }

    render() {
        return (
            <Flex flexDirection="column" align="center">
                <LoginScreen />
            </Flex>
        )
    }
}
