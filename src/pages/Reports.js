import React, { Component } from 'react'
import FontAwesome from 'react-fontawesome'
import { Flex, Box, Text, Heading, Link } from '@chakra-ui/core'
import { REACT_APP_HOST } from '../config'
import { Mapa } from '../Components/Mapa'
import './Reports.css'
import axios from 'axios'

export default class Reports extends Component {
    constructor(props) {
        super(props)
        this.state = {
            markers: [], usuario: sessionStorage.getItem('usuarioId'),
            error: false, errData: {}, isLoading: true
        }
    }

    componentDidMount = async () => {
        const token = localStorage.getItem("token")
        await axios.get(`${ REACT_APP_HOST }/report`, { headers: { "Authorization": `Bearer ${ token }` } })
            .then(res => this.setState({ markers: res.data, isLoading: false }))
            .catch(err => this.setState({ error: true, errData: { message: err.response.data, status: err.response.status, isLoading: false } }))
    }

    render() {
        const { markers, error, errData, isLoading } = this.state
        const colors = { "0": "#03a9f4", "1": "#4caf50", "2": "#ffc107", "3": "#ff5722", "4": "#f44336" }

        if (error) {
            return (<div>
                <h4>Error { errData.status }</h4>
                <p>{ errData.message }</p>
            </div>)
        }

        if (isLoading) {
            return (
                <div>
                    <div style={{ width: "100%", height: "100%", backgroundColor: "#ffffffdd", zIndex: 10 }}>
                        <FontAwesome className="spinner-loader" name="spinner" spin size="5x" />
                    </div>
                </div>
            )
        }

        const markersComponent = []
        if (markers) {
            let editorPencil = props => (<Link to={ `/report/${ props.url }` } style={{ textDecorationLine: "none" }}>
                    <FontAwesome className="pencil-editor" name="pencil" size="2x" pr={ 2 } />
                </Link>)
            markers.map(data => markersComponent.push(<Box rounded="lg" overflow="hidden" bg={ colors[data.impacto] } color="white" style={{ margin: "1em", marginTop: "0.5em", marginBottom: "0.5em", padding: "1em" }}>
                    <Heading as="h5">{ data.descripcion }</Heading>
                    <p lineHeight="tight">{ data.fecha.replace('T', " • ") }</p>
                    { data._id == sessionStorage.getItem('usuarioId') ? editorPencil : "" }
                </Box>
            ))
        }

        return (
            <Flex flexDirection="row">
                <Box style={{ right: "0", top: "0", width: window.innerWidth }}>
                    <Mapa markers={ markers } lat={ 20.5914079 } lon={ -100.4100358 } height={ window.innerHeight } />
                </Box>
                <Flex flexDirection="column" bg="#ddd" maxWidth="420px" style={{ left: "0", right: "0", top: "0" }}>
                    <Heading as="h3" textAlign={['left', 'center']} >
                        Marcadores
                    </Heading>
                    { markersComponent }
                </Flex>
            </Flex>
        )
    }
}
