import './Home.css'
import React, { Component } from 'react'

export default class Home extends Component {
    render() {
        return (
            <div className="container">
                <h2>HeyListen</h2>
                <img src="/img/heylisten_icon.png" />
            </div>
        )
    }
}
