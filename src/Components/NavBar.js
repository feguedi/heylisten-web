import React from 'react'
import { theme, ThemeProvider, Box, Flex, Text, Heading, Link, Button } from '@chakra-ui/core'
import './NavBar.css'


const navBar = sessionStorage.getItem("token") ? 
    (<Box flexDirection="row">
        <ul className="nav navbar-nav">
            <li className="nav-item" role="presentation"><Link className="nav-link" to="/login">Entrar</Link></li>
            <li className="nav-item" role="presentation"><Link className="nav-link" to="/signup">Registrarme</Link></li>
        </ul>
    </Box>) : 
    (<Box flexDirection="row" right="0" whiteSpace={ 20 } paddingRight={ 0 }>
        <ul className="nav navbar-nav">
            <li className="nav-item" role="presentation"><Link className="nav-link" to="/reports">Reportes</Link></li>
            <li className="nav-item" role="presentation"><Link className="nav-link" to="/profile">Perfil</Link></li>
        </ul>
    </Box>
    )

export const NavBar = () => (
    <Box className="navbar navbar-default navbar-fixed-top" >
        <ThemeProvider theme={ theme } className="container">
            <Flex flexDirection="row">
                <Link to="/" className="navbar-brand">
                    <Heading as="h5">HeyListen</Heading>
                </Link>
                <div className="collapse navbar-collapse d-flex" id="navcol-1">
                    { navBar }
                </div>
            </Flex>
        </ThemeProvider>
    </Box>
)
