import React, { Component } from 'react'
import FontAwesome from 'react-fontawesome'
import { Mapa } from './Mapa'
import { Button } from '@chakra-ui/core'
import axios from 'axios'

export default class Vista extends Component {
    constructor(props) {
        super(props)
        this.state = { error: false, errMsg: "", isLoading: true, lon: "", lat: "" }
        setTimeout(() => {
            this.setData()
        }, 400)
    }

    setData = async () => {
        setTimeout(() => {
            this.setState({ lat: 20.59220, lon: -100.40988, isLoading: false })
        }, 1000)
        // await axios.get('https://api.wheretheiss.at/v1/satellites/25544')
        //     .then(res => this.setState({ lon: res.data.longitude, lat: res.data.latitude, isLoading: false }))
        //     .catch(err => this.setState({ error: true, errMsg: err.message, isLoading: false }))
    }

    render() {
        const { errMsg, error, isLoading, lat, lon } = this.state

        const loader = (<img style={{ width: "300px", height: "18px" }} src="img/loader.gif" />)

        const layout = () => (<div>
                <h3>Datos</h3>
                <p style={{ justifyContent: "center", alignContent: "center", alignItems: "center" }}>Latitud: { loader }</p>
                <p style={{ justifyContent: "center", alignContent: "center", alignItems: "center" }}>Longitud: { loader }</p>
                <Button variantColor="teal" size="sm" onClick={() => console.log(`Presionado el botón del mapa`)}>Mapa</Button>
            </div>)

        if (error) {
            return (<div>
                <h4>Error</h4>
                <p>{ errMsg }</p>
            </div>)
        }

        if (isLoading) {
            return (
                <div>
                    <div style={{ width: "100%", height: "100%", backgroundColor: "#ffffff44", z: 10 }}>
                        <FontAwesome style={{ position: "absolute", right: "48%", top: "48%" }} name="spinner" spin size="5x" />
                    </div>
                    { layout() }
                </div>
            )
        }

        return (
            <div>
                <h3>Datos</h3>
                <p>Latitud: { lat }</p>
                <p>Longitud: { lon }</p>
                <Button variantColor="teal" size="lg" onClick={() => console.log(`Presionado el botón del mapa`)}>Mapa</Button>
                <div className="container">
                    <Mapa lat={ lat } lon={ lon } />
                </div>
            </div>
        )
    }
}
