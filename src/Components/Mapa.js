import React from 'react'
import { Map, Marker, Popup, TileLayer, CircleMarker, Circle } from 'react-leaflet'

const getCenter = coord => {
    const { lat, lon, markers } = coord
    let lat_ = 0, lon_ = 0
    const _len = markers ? markers.length : undefined
    if (lat && lon) {
        return [lat, lon]
    } else {
        switch (_len) {
            case 1:
                return [markers[0].latitud, markers[0].longitud]
            case 2:
                lat_ = (markers[0].latitud + markers[1].latitud) / 2
                lon_ = (markers[0].longitud + markers[1].longitud) / 2
                return [lat_, lon_]
            default:
                let idx = 0
                for (const marker of markers) {
                    lat_ += marker.latitud
                    lon_ += marker.longitud
                    idx++
                }
                lat_ /= idx
                lon_ /= idx
                return [lat_, lon_]
        }
    }
}

export const Mapa = props => {
    const position = getCenter({ lat: props.lat, lon: props.lon, markers: props.markers })
    const markers = props.markers
    const url = 'https://{s}.tile.osm.org/{z}/{x}/{y}.png'
    const attribution = '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'

    const colors = { "0": "blue", "1": "green", "2": "yellow", "3": "orange", "4": "red" }
    const markersComponents = []
    if (markers) {
        markers.map(data => {
            console.log(`Datos del marcador: ${ JSON.stringify(data) }`)
            markersComponents.push(<CircleMarker 
                
                fillColor={ colors[data.impacto] }
                center={ [data.latitud, data.longitud] } 
                radius={ 10 }>
                    <Popup>
                        { data.fecha }<br />{ data.descripcion }
                    </Popup>
                </CircleMarker>)
        })
    }
    return(<Map style={{ width: "100%", height: props.height }} center={ position } zoom={ 17 }>
        <TileLayer
            attribution={ attribution }
            url={ url }
        />
        { markersComponents }
    </Map>)
}