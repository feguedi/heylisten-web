const { config } = require('dotenv')
config({ silent: true })

module.exports = {
    REACT_APP_NODE_ENV: process.env.REACT_APP_NODE_ENV,
    REACT_APP_HOST: process.env.REACT_APP_HOST
}